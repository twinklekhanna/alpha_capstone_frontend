import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SigninComponent } from './account/login/signin/signin.component';
import { RegisterComponent } from './account/signup/register/register.component';
import { VieworderComponent } from './checkout/order/vieworder/vieworder.component';
import { AddpaymentComponent } from './checkout/payment/addpayment/addpayment.component';
import { HomeComponent } from './home/home/home.component';
import { AddcategoryComponent } from './shop/category/addcategory/addcategory.component';
import { AddproductComponent } from './shop/product/addproduct/addproduct.component';
import { ViewproductComponent } from './shop/product/viewproduct/viewproduct.component';

const routes: Routes = [
  {path: 'view-products', component: ViewproductComponent},
  {path: 'add-product', component: AddproductComponent},
  {path: 'add-category', component: AddcategoryComponent},
  {path: 'signup', component: RegisterComponent},
  {path: 'login', component: SigninComponent},
  {path: 'order', component: VieworderComponent},
  {path: 'payment', component: AddpaymentComponent },
  {path: 'home', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
