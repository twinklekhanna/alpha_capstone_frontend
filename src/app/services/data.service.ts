import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  sharedData: any;
  order : any;

  constructor() { }

  setData(data: any) {
    this.sharedData = data;
  }

  getData(){
    return this.sharedData;
  }

  setOrder(order: any) {
    this.order = order;
  }

  getOrder() {
    return this.order;
  }
}
