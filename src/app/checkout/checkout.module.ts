import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VieworderComponent } from './order/vieworder/vieworder.component';
import { AddpaymentComponent } from './payment/addpayment/addpayment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    VieworderComponent,
    AddpaymentComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CheckoutModule { }
