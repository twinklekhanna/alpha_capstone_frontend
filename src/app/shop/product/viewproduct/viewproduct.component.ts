import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { EcomService } from 'src/app/services/ecom.service';

@Component({
  selector: 'app-viewproduct',
  templateUrl: './viewproduct.component.html',
  styleUrls: ['./viewproduct.component.scss']
})
export class ViewproductComponent implements OnInit {

  constructor(private apiService: EcomService, private formbuilder: FormBuilder, private router: Router, private sharedata: DataService) { }

  data: any;
  customerdetails: any;
  paymentdetails: any;
  order: any;
  categories: any;
  trackByIndex = (index:number):number =>{
    return index;
  }

  myForm = this.formbuilder.group({
    OrderQuantity : ['', Validators.required],
    mySelect : ['', Validators.required]
  });
  ngOnInit(): void {
    this.apiService.getProducts().subscribe(response => {
      this.data = response;
      console.log(this.data);
  })
  this.customerdetails = JSON.parse(localStorage.getItem('customer')||"{}");
  this.apiService.getCategories().subscribe(response => {
    this.categories = response;
  });
}

addorder(index: number) {
  let price : any =  this.myForm.value.OrderQuantity;

  let order = {
    ProductId : this.data[index].productId,
    CustomerId : this.customerdetails.customerId, 
    OrderQuantity : this.myForm.value.OrderQuantity,
    OrderPrice : this.data[index].productPrice * price,
    ShipmentAddress : this.customerdetails.customerAddress
  }

  this.sharedata.setOrder(order);

  this.router.navigateByUrl('/payment');
}

filter() {
  if(this.myForm.value.mySelect == "All") {
    this.apiService.getProducts().subscribe(response => {
      this.data = response;
      console.log("All",this.data)
    })
  }
  else {
    this.apiService.getProductByCategoryId(this.myForm.value.mySelect).subscribe(response => {
      this.data = response;
      console.log("ByCategory",this.data)
    });
  }
}
}
