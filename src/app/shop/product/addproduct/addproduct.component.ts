import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { EcomService } from 'src/app/services/ecom.service';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.scss']
})
export class AddproductComponent implements OnInit {

  constructor(private apiService: EcomService, private formbuilder: FormBuilder) { }

  myform = this.formbuilder.group({
    CategoryId : ['', Validators.required],
    ProductName : ['', Validators.required],
    ProductType : ['', Validators.required],
    ProductPrice : ['', Validators.required],
    ProductDescription : ['', Validators.required],
    ProductImageUrl : ['', Validators.required],
  })

  ngOnInit(): void {
  }

  add(){
    if(this.myform.status == 'VALID'){
      let product = {
        CategoryId : this.myform.value.CategoryId,
        ProductName : this.myform.value.ProductName,
        ProductType : this.myform.value.ProductType,
        ProductPrice : this.myform.value.ProductPrice,
        ProductDescription : this.myform.value.ProductDescription,
        ProductImageUrl : this.myform.value.ProductImageUrl
      }

      this.apiService.addProduct(product).subscribe(response => {
        console.log(response);
      })

      alert('Submitted successfully');
    }

    else{
      alert('All fields are required.');
    }
  }

}
